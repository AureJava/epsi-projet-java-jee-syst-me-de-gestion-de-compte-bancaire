package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author nbuser
 */
@Entity
@Table(name = "retrait")
@NamedQueries({
    @NamedQuery(name = "Retrait.findAll", query = "SELECT r FROM Retrait r"),
    @NamedQuery(name = "Retrait.findByRetraitId", query = "SELECT r FROM Retrait r WHERE r.retraitId = :retraitId"),
    @NamedQuery(name = "Retrait.findByCompteId", query = "SELECT r FROM Retrait r WHERE r.compteId = :compteId"),
    @NamedQuery(name = "Retrait.findByRetrait", query = "SELECT r FROM Retrait r WHERE r.retrait = :retrait"),
    @NamedQuery(name = "Retrait.findByFactureId", query = "SELECT r FROM Retrait r WHERE r.factureId = :factureId")})
public class Retrait implements Serializable {
    private static final long serialVersionUID = 1L;
//    @EmbeddedId
//    protected RetraitPK projectPK;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "retrait_id")
    private Integer retraitId;
    @JoinColumn(name = "compte_id", referencedColumnName = "compte_id")
    @ManyToOne(optional = false)
    private Compte compteId;
    @Column(name = "retrait")
    private Double retrait;
    @JoinColumn(name = "facture_id", referencedColumnName = "facture_id")
    @ManyToOne(optional = false)
    private Facture factureId;
    //public Compte compte;
//    public Compte solde;
//    String soldestr = solde.toString(); 
//    Double soldeconv = Double.valueOf(soldestr).doubleValue();
    
     void retraitSolde(int compteid, double retrait){
        double nouveauSolde = compteId.getSolde() - retrait;
        compteId.ModifCompte(compteid, nouveauSolde);
    }

    public Retrait(Integer retraitId, Compte compteId, Double retrait, Facture factureId) {
        this.retraitId = retraitId;
        this.compteId = compteId;
        this.retrait = retrait;
        this.factureId = factureId;
    }
             
 //       public Compte getCompte() {
 //           return compte;
 //       }

  //      public void setCompte(Compte compte) {
  //          this.compte = compte;
  //      }

    

//    public String getSoldestr() {
//        return soldestr;
//    }
//
//    public void setSoldestr(String soldestr) {
//        this.soldestr = soldestr;
//    }
//
//    public Double getSoldeconv() {
//        return soldeconv;
//    }
//
//    public void setSoldeconv(Double soldeconv) {
//        this.soldeconv =soldeconv - retrait;
//    }
    
    
    
    
    
    public Facture getFactureId() {
        return factureId;
    }

    public void setFactureId(Facture factureId) {
        this.factureId = factureId;
    }

    public Retrait() {
    }

    public Retrait(Integer retraitId) {
        this.retraitId = retraitId;
    }
    
    

    public Integer getRetraitId() {
        return retraitId;
    }

    public void setRetraitId(Integer retraitId) {
        this.retraitId = retraitId;
    }

    public Compte getCompteId() {
        return compteId;
    }

    public void setCompteId(Compte compteId) {
        this.compteId = compteId;
    }

    public Double getRetrait() {
        return retrait;
    }

    public void setRetrait(Double retrait) {
        this.retrait = retrait;
    }

    

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (retraitId != null ? retraitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retrait)) {
            return false;
        }
        Retrait other = (Retrait) object;
        if ((this.retraitId == null && other.retraitId != null) || (this.retraitId != null && !this.retraitId.equals(other.retraitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Retrait[retraitId=" + retraitId + "]";
    }

}
