package jpa.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author aurelien
 */
@Entity
@Table(name = "compte")
@NamedQueries({
    @NamedQuery(name = "Compte.findAll", query = "SELECT c FROM Compte c"),
    @NamedQuery(name = "Compte.findByCompteId", query = "SELECT c FROM Compte c WHERE c.compteId = :compteId"),
    @NamedQuery(name = "Compte.findByPrenom", query = "SELECT c FROM Compte c WHERE c.prenom = :prenom"),
    @NamedQuery(name = "Compte.findByNom", query = "SELECT c FROM Compte c WHERE c.nom = :nom"),
    @NamedQuery(name = "Compte.findByNumeroCarteBancaire", query = "SELECT c FROM Compte c WHERE c.numerocartebancaire = :numerocartebancaire"),
    @NamedQuery(name = "Compte.findByNumeroIban", query = "SELECT c FROM Compte c WHERE c.numeroiban = :numeroiban"),
    @NamedQuery(name = "Compte.findByNumeroBic", query = "SELECT c FROM Compte c WHERE c.numerobic = :numerobic"),
    @NamedQuery(name = "Compte.findBySolde", query = "SELECT c FROM Compte c WHERE c.solde = :solde")})
public class Compte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "compte_id")
    private Integer compteId;
    
    @Basic(optional = false)
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "nom")
    private String nom;
    @Basic(optional = false)
    @Column(name = "numero_carte_bancaire")
    private Integer numero_carte_bancaire;
    @Basic(optional = false)
    @Column(name = "numero_iban")
    private Integer numeroiban;
    @Basic(optional = false)
    @Column(name = "numero_bic")
    private Integer numerobic;
    @Basic(optional = false)
    @Column(name = "solde")
    public Double solde;
    
     private Connection connect() 
    {
        String url = "jdbc:mysql://localhost:3306/db_comptes";
        String user = "root";
        String password = "backend";
        Connection connection;
        try 
        {
            connection = DriverManager.getConnection(url, user, password);
        } 
        catch (SQLException e) 
        {
            connection = null;
        }
        return connection;
    }
    
     boolean ModifCompte(int compteId, Double nouveauSolde){
        boolean success = false;
        Connection connection = connect();
        try{
            PreparedStatement modifSolde = connection.prepareStatement(
                    "UPDATE Compte SET solde = ? WHERE compteId = ?");
            
                modifSolde.setDouble(1, nouveauSolde);
                modifSolde.setInt(2, compteId);
                modifSolde.executeUpdate();
            success = true;
        } catch (SQLException ex) {
            System.err.println("An error has occured." + ex.getMessage());
        }
        return success;
    }
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compteId")
    private Collection<Depot> versementCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compteId")
    private Collection<Retrait> retraitCollection;

    public Collection<Depot> getVersementCollection() {
        return versementCollection;
    }

    public void setVersementCollection(Collection<Depot> versementCollection) {
        this.versementCollection = versementCollection;
    }

    public Collection<Retrait> getRetraitCollection() {
        return retraitCollection;
    }

    public void setRetraitCollection(Collection<Retrait> retraitCollection) {
        this.retraitCollection = retraitCollection;
    }

    
    
    public Compte() {
    }

    public Compte(Integer compteId) {
        this.compteId = compteId;
    }

    public Compte(Integer compteId, String prenom, String nom, Integer numerocartebancaire, Integer numeroiban, Integer numerobic, Double solde, Collection<Depot> versementCollection, Collection<Retrait> retraitCollection) {
        this.compteId = compteId;
        this.prenom = prenom;
        this.nom = nom;
        this.numero_carte_bancaire = numerocartebancaire;
        this.numeroiban = numeroiban;
        this.numerobic = numerobic;
        this.solde = solde;
        this.versementCollection = versementCollection;
        this.retraitCollection = retraitCollection;
    }

    

    public Integer getCompteId() {
        return compteId;
    }

    public void setCompteId(Integer compteId) {
        this.compteId = compteId;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNumeroiban() {
        return numeroiban;
    }

    public Integer getNumerocartebancaire() {
        return numero_carte_bancaire;
    }

    public void setNumerocartebancaire(Integer numerocartebancaire) {
        this.numero_carte_bancaire = numerocartebancaire;
    }

    public void setNumeroiban(Integer numeroiban) {
        this.numeroiban = numeroiban;
    }

    public Integer getNumerobic() {
        return numerobic;
    }

    public void setNumerobic(Integer numerobic) {
        this.numerobic = numerobic;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compteId != null ? compteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compte)) {
            return false;
        }
        Compte other = (Compte) object;
        if ((this.compteId == null && other.compteId != null) || (this.compteId != null && !this.compteId.equals(other.compteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Compte[compteId=" + compteId + "]";
    }

}
