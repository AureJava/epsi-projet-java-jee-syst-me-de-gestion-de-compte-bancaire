package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author nbuser
 */
@Entity
@Table(name = "facture")
@NamedQueries({
    @NamedQuery(name = "Facture.findAll", query = "SELECT f FROM Facture f"),
    @NamedQuery(name = "Facture.findByFactureId", query = "SELECT f FROM Facture f WHERE f.factureId = :factureId"),
    @NamedQuery(name = "Facture.findByCompteId", query = "SELECT f FROM Facture f WHERE f.compteId = :compteId"),
    @NamedQuery(name = "Facture.findByDepotId", query = "SELECT f FROM Facture f WHERE f.depotId = :depotId"),
    @NamedQuery(name = "Facture.findByRetraitId", query = "SELECT f FROM Facture f WHERE f.retraitId = :retraitId"),
    @NamedQuery(name = "Facture.findBySolde", query = "SELECT f FROM Facture f WHERE f.solde = :solde"),
    @NamedQuery(name = "Facture.findByVersement", query = "SELECT f FROM Facture f WHERE f.versement = :versement"),
    @NamedQuery(name = "Facture.findByRetrait", query = "SELECT f FROM Facture f WHERE f.retrait = :retrait"),
    @NamedQuery(name = "Facture.findByDescription", query = "SELECT f FROM Facture f WHERE f.description = :description")
})
public class Facture implements Serializable {
    private static final long serialVersionUID = 1L;
//    @EmbeddedId
//    protected FacturePK facturePK;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "facture_id")
    private Integer factureId;
    @JoinColumn(name = "compte_id", referencedColumnName = "compte_id")
    @OneToOne(optional = false)
    private Compte compteId;
    @JoinColumn(name = "depot_id", referencedColumnName = "depot_id")
    @OneToOne(optional = false)
    private Depot depotId;
    @JoinColumn(name = "retrait_id", referencedColumnName = "retrait_id")
    @OneToOne(optional = false)
    private Retrait retraitId;
    @Column(name = "solde")
    private Double solde;
    @Column(name = "versement")
    private Double versement;
    @Column(name = "retrait")
    private Double retrait;
    @Column(name = "description")
    private String description;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "factureId")
    private Collection<Depot> versementCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "factureId")
    private Collection<Retrait> retraitCollection;
 
    

    public Facture() {
    }

    public Facture(Integer factureId) {
        this.factureId = factureId;
    }

    public Facture(Integer factureId, Compte compteId, Depot depotId,Retrait retraitId,Double solde, Double versement, Double retrait, String description, Collection<Depot> versementCollection, Collection<Retrait> retraitCollection) {
        this.factureId = factureId;
        this.compteId = compteId;  
        this.retraitId=retraitId;
        this.depotId=depotId;
        this.solde = solde;
        this.versement = versement;
        this.retrait = retrait;
        this.versement = versement;
        this.description = description;
        this.versementCollection = versementCollection;
        this.retraitCollection = retraitCollection;
    }

    public Depot getDepotId() {
        return depotId;
    }

    public void setDepotId(Depot depotId) {
        this.depotId = depotId;
    }

    public Retrait getRetraitId() {
        return retraitId;
    }

    public void setRetraitId(Retrait retraitId) {
        this.retraitId = retraitId;
    }
    
    
    
    public Integer getFactureId() {
        return factureId;
    }

    public void setFactureId(Integer factureId) {
        this.factureId = factureId;
    }

    public Compte getCompteId() {
        return compteId;
    }

    public void setCompteId(Compte compteId) {
        this.compteId = compteId;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Double getVersement() {
        return versement;
    }

    public void setVersement(Double versement) {
        this.versement = versement;
    }

    public Double getRetrait() {
        return retrait;
    }

    public void setRetrait(Double retrait) {
        this.retrait = retrait;
    }

    public Double getDepot() {
        return versement;
    }

    public void setDepot(Double versement) {
        this.versement = versement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Depot> getVersementCollection() {
        return versementCollection;
    }

    public void setVersementCollection(Collection<Depot> versementCollection) {
        this.versementCollection = versementCollection;
    }

    public Collection<Retrait> getRetraitCollection() {
        return retraitCollection;
    }

    public void setRetraitCollection(Collection<Retrait> retraitCollection) {
        this.retraitCollection = retraitCollection;
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (factureId != null ? factureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facture)) {
            return false;
        }
        Facture other = (Facture) object;
        if ((this.factureId == null && other.factureId != null) || (this.factureId != null && !this.factureId.equals(other.factureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Facture[factureId=" + factureId + "]";
    }

}
