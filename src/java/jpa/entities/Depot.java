package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nbuser
 */
@Entity
@Table(name = "depot")
@NamedQueries({
    @NamedQuery(name = "Depot.findAll", query = "SELECT d FROM Depot d"),
    @NamedQuery(name = "Depot.findByDepotId", query = "SELECT d FROM Depot d WHERE d.depotId = :depotId"),
    @NamedQuery(name = "Depot.findByCompteId", query = "SELECT d FROM Depot d WHERE d.compteId = :compteId"),
    @NamedQuery(name = "Depot.findByVersement", query = "SELECT d FROM Depot d WHERE d.versement = :versement"),
    @NamedQuery(name = "Depot.findByConsultantId", query = "SELECT d FROM Depot d WHERE d.consultantId = :consultantId"),
    @NamedQuery(name = "Depot.findByFactureId", query = "SELECT d FROM Depot d WHERE d.factureId = :factureId"),
    })
public class Depot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "depot_id")
    private Long depotId;
    @Column(name = "versement")
    private Double versement ;
    @JoinColumn(name = "compte_id", referencedColumnName = "compte_id")
    @ManyToOne(optional = false)
    private Compte compteId;
    @JoinColumn(name = "consultant_id", referencedColumnName = "consultant_id")
    @ManyToOne(optional = false)
    private Consultant consultantId;
    
    //public Compte compte;
    
    @JoinColumn(name = "facture_id", referencedColumnName = "facture_id")
    @ManyToOne(optional = false)
    private Facture factureId;

    public Depot(Long depotId, Double versement, Compte compteId, Consultant consultantId, Compte compte, Facture factureId) {
        this.depotId = depotId;
        this.versement = versement;
        this.compteId = compteId;
        this.consultantId = consultantId;
        //this.compte = compte;
        this.factureId = factureId;
    }

    
    
     void depotSolde(int compteid, double versement) throws VersementInvalideException {
        if (versement <= 0) {
            throw new VersementInvalideException();
        }
        double nouveauSolde = compteId.getSolde() + versement;
        compteId.ModifCompte(compteid, nouveauSolde);
    }

//    public Compte getCompte() {
//        return compte;
//    }
//
//    public void setCompte(Compte compte) {
//        this.compte = compte;
//    }
    
     
     
    public Facture getFactureId() {
        return factureId;
    }

    public void setFactureId(Facture factureId) {
        this.factureId = factureId;
    }

    public Depot() {
    }

    public Long getDepotId() {
        return depotId;
    }

    public void setDepotId(Long depotId) {
        this.depotId = depotId;
    }

    public Double getVersement() {
        return versement;
    }

    public void setVersement(Double versement) {
        this.versement = versement;
    }

    public Compte getCompteId() {
        return compteId;
    }

    public void setCompteId(Compte compteId) {
        this.compteId = compteId;
    }

    public Consultant getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Consultant consultantId) {
        this.consultantId = consultantId;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (depotId != null ? depotId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Depot)) {
            return false;
        }
        Depot other = (Depot) object;
        if ((this.depotId == null && other.depotId != null) || (this.depotId != null && !this.depotId.equals(other.depotId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Depot[depotId=" + depotId + "]";
    }

}
