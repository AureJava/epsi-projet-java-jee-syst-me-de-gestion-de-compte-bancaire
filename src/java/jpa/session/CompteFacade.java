package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.entities.Compte;

/**
 *
 * @author nbuser
 */
@Stateless
public class CompteFacade {
    @PersistenceContext(unitName = "CompteBanquePU")
    private EntityManager em;

    public void create(Compte compte) {
        em.persist(compte);
    }

    public void edit(Compte compte) {
        em.merge(compte);
    }

    public void remove(Compte compte) {
        em.remove(em.merge(compte));
    }

    public Compte find(Object id) {
        return em.find(Compte.class, id);
    }

    public List<Compte> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Compte.class));
        return em.createQuery(cq).getResultList();
    }

    public List<Compte> findRange(int[] range) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Compte.class));
        Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Compte> rt = cq.from(Compte.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
