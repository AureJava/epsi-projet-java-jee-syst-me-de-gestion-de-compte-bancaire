package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.entities.Depot;

/**
 *
 * @author nbuser
 */
@Stateless
public class DepotFacade {
    @PersistenceContext(unitName = "CompteBanquePU")
    private EntityManager em;

    public void create(Depot depot) {
        em.persist(depot);
    }

    public void edit(Depot depot) {
        em.merge(depot);
    }

    public void remove(Depot depot) {
        em.remove(em.merge(depot));
    }

    public Depot find(Object id) {
        return em.find(Depot.class, id);
    }

    public List<Depot> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Depot.class));
        return em.createQuery(cq).getResultList();
    }

    public List<Depot> findRange(int[] range) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Depot.class));
        Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Depot> rt = cq.from(Depot.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
