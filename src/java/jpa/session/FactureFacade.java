package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.entities.Facture;

/**
 *
 * @author nbuser
 */
@Stateless
public class FactureFacade {
    @PersistenceContext(unitName = "CompteBanquePU")
    private EntityManager em;

    public void create(Facture facture) {
        em.persist(facture);
    }

    public void edit(Facture facture) {
        em.merge(facture);
    }

    public void remove(Facture facture) {
        em.remove(em.merge(facture));
    }

    public Facture find(Object id) {
        return em.find(Facture.class, id);
    }

    public List<Facture> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Facture.class));
        return em.createQuery(cq).getResultList();
    }

    public List<Facture> findRange(int[] range) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Facture.class));
        Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Facture> rt = cq.from(Facture.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
