package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.entities.Retrait;

/**
 *
 * @author nbuser
 */
@Stateless
public class RetraitFacade {
    @PersistenceContext(unitName = "CompteBanquePU")
    private EntityManager em;

    public void create(Retrait retrait) {
        em.persist(retrait);
    }

    public void edit(Retrait retrait) {
        em.merge(retrait);
    }

    public void remove(Retrait retrait) {
        em.remove(em.merge(retrait));
    }

    public Retrait find(Object id) {
        return em.find(Retrait.class, id);
    }

    public List<Retrait> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Retrait.class));
        return em.createQuery(cq).getResultList();
    }

    public List<Retrait> findRange(int[] range) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Retrait.class));
        Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Retrait> rt = cq.from(Retrait.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
